unit uFazedorAcoes;

interface

uses IInterfaceA, IInterfaceB;

type
  TFazedorDeAcao = class(TInterfacedObject,InterfaceA,InterfaceB)

  public
    function FazerAcaoB: string;
    function FazAcaoA: string;
  end;

implementation

{ TFazedorDeAcao }

function TFazedorDeAcao.FazAcaoA: string;
begin
  result := 'Fazendo a fun��o A';
end;

function TFazedorDeAcao.FazerAcaoB: string;
begin
  result := 'Fazendo a fun��o B';
end;

end.
