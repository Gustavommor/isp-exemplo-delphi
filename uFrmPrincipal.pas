unit uFrmPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, uFazedorAcoes, uController;

type
  TFrmPrincipal = class(TForm)
    btnAcaoA: TButton;
    btnAcaoB: TButton;
    procedure btnAcaoAClick(Sender: TObject);
    procedure btnAcaoBClick(Sender: TObject);
  private
    { Private declarations }
    FFazedor : TFazedorDeAcao;
    FController : TController;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

{$R *.dfm}

{ TFrmPrincipal }

procedure TFrmPrincipal.btnAcaoAClick(Sender: TObject);
begin
  ShowMessage(FController.AcaoA(FFazedor));
  ShowMessage(FController.AcaoB(FFazedor));
end;

procedure TFrmPrincipal.btnAcaoBClick(Sender: TObject);
begin
  ShowMessage(FController.AcaoB(FFazedor));
end;

constructor TFrmPrincipal.Create(AOwner: TComponent);
begin
  inherited;
  FFazedor := TFazedorDeAcao.Create;
  FController := TController.Create;
end;

destructor TFrmPrincipal.Destroy;
begin
  FreeAndNil(FFazedor);
  FreeAndNil(FController);
  inherited;
end;

end.
