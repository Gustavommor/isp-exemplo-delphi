program ExemploISP;

uses
  Vcl.Forms,
  uFrmPrincipal in 'uFrmPrincipal.pas' {FrmPrincipal},
  IInterfaceA in 'IInterfaceA.pas',
  IInterfaceB in 'IInterfaceB.pas',
  uFazedorAcoes in 'uFazedorAcoes.pas',
  uController in 'uController.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.Run;
end.
