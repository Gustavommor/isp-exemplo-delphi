unit uController;

interface

uses IInterfaceA, IInterfaceB;

type
  TController = class
  private

  public
    function AcaoA(pFazedor : InterfaceA) : string;
    function AcaoB(pFazedor : InterfaceB) : string;
  end;

implementation

{ TController }

function TController.AcaoA(pFazedor: InterfaceA): string;
begin
  result := pFazedor.FazAcaoA();
end;

function TController.AcaoB(pFazedor: InterfaceB): string;
begin
  result := pFazedor.FazerAcaoB();
end;

end.
