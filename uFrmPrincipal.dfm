object FrmPrincipal: TFrmPrincipal
  Left = 0
  Top = 0
  Caption = 'Principal'
  ClientHeight = 67
  ClientWidth = 247
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnAcaoA: TButton
    Left = 24
    Top = 24
    Width = 75
    Height = 25
    Caption = 'A'#231#227'o A'
    TabOrder = 0
    OnClick = btnAcaoAClick
  end
  object btnAcaoB: TButton
    Left = 136
    Top = 24
    Width = 75
    Height = 25
    Caption = 'A'#231#227'o B'
    TabOrder = 1
    OnClick = btnAcaoBClick
  end
end
